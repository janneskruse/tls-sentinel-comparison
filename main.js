window.addEventListener("load", function() {    
    
    //create a costum cursor
    //smooth rendering
    // set the starting position of the cursor outside of the screen
    let clientX = -100;
    let clientY = -100;
    const cursor = document.querySelector(".cursor");
    const initCursor = (cursoritem) => {
      // add listener to track the current mouse position
      document.addEventListener("mousemove", e => {
        clientX = e.clientX;
        clientY = e.clientY;
    });  
      // transform the innerCursor to the current mouse position
      // use requestAnimationFrame() for smooth performance
    const render = () => {
        cursoritem.style.transform = `translate(${clientX}px, ${clientY}px)`;
        requestAnimationFrame(render);
      };
      requestAnimationFrame(render);
    };
    initCursor(cursor);
    
    //adding the css classes for links and other exceptions
    function csscursor (cursoritem) {
    const linkItems = document.querySelectorAll("a, summary, link-item, .anchorjs-link:hover");
    linkItems.forEach(item => {
      item.addEventListener("mouseenter", e => {
     cursoritem.classList.add("cursorhover");
    }); 
    item.addEventListener("mouseleave", e => {
     cursoritem.classList.remove("cursorhover");
    }); 
    });
    const quartheader= document.querySelectorAll('.quarto-title-banner');
    quartheader.forEach(item => {
      item.addEventListener("mouseenter", e => {
     cursoritem.classList.add("cursorwhite");
    }); 
    item.addEventListener("mouseleave", e => {
     cursoritem.classList.remove("cursorwhite");
    }); 
    });
    }
    csscursor(cursor);
    
    
//    // Add dot background to title-block
//    // Get the original element
//    let original = document.querySelector(".quarto-title-banner");
//    // Create a new element with the same size as the original
//    let newEl = document.createElement("div");
//    newEl.style.height = original.clientHeight + "px";
//    newEl.style.width = original.clientWidth + "px";
//    // Add a new class and id to the new element
//    newEl.classList.add("dot");
//    newEl.classList.add("page-columns","page-full"); //add position classes to it
//    newEl.id = "dotted";
//    // Insert the new element before the original
//    original.insertAdjacentElement("beforebegin", newEl);
//    // update new element size
//    function updateSize() {
//        newEl.style.height = original.clientHeight + "px";
//        newEl.style.width = original.clientWidth + "px";
//    }
//    updateSize();
//    // Listen for resize events
//    window.addEventListener("resize", updateSize);
    
    
    
//    
//    var trElements = document.querySelectorAll("tr");
//    trElements.forEach(function(tr) {
//      tr.classList.remove("header");
//    });

    
    
    
});
    
